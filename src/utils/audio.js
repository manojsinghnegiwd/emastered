export const convertURIToBinary = (dataURI) => {
    let BASE64_MARKER = 'base64,'
    let base64Index = dataURI.indexOf(BASE64_MARKER) + BASE64_MARKER.length
    let base64 = dataURI.substring(base64Index)
    let raw = window.atob(base64)
    let rawLength = raw.length
    let arr = new Uint8Array(new ArrayBuffer(rawLength))

    for (let i = 0; i < rawLength; i++) {
        arr[i] = raw.charCodeAt(i)
    }

    return arr
}

export const downloadFile = (filename, blob) => {
    const element = document.createElement("a")
    element.download = filename
    element.href = URL.createObjectURL(blob)
    element.style.display = "none"
    document.body.appendChild(element)
    element.click()
    document.body.removeChild(element)
}
