import React from 'react';
import { render } from '@testing-library/react';
import Download from '../components/Audio/Download';
import { TEST_AUDIO } from '../utils/constant';

test('Download button renders empty if audiotrack is null', () => {
  const { queryByText } = render(
    <Download onDownload={() => {}} audioTrack={null} />
  );

  const downloadButton = queryByText('DOWNLOAD')

  expect(downloadButton).toBeNull();
});

test('Download button renders if audiotrack is provided', () => {
    const audioTrack = JSON.stringify(TEST_AUDIO)

    const { getByText } = render(
        <Download onDownload={() => {}} audioTrack={audioTrack} />
    );

    const buttonText = getByText(/download/i);
    expect(buttonText).toBeInTheDocument();
});

test('Download button renders filename if audiotrack is provided', () => {
    const audioTrack = JSON.stringify(TEST_AUDIO)

    const { getByText } = render(
        <Download onDownload={() => {}} audioTrack={audioTrack} />
    );

    const filename = getByText("file_example_MP3_2MG.mp3");
    expect(filename).toBeInTheDocument();
});

