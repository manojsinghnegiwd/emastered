import React from 'react';
import { render, fireEvent, queryByAttribute } from '@testing-library/react';
import Download from '../components/Audio/Download';
import { TEST_AUDIO } from '../utils/constant';
import Player from '../components/Audio/Player';

const getById = queryByAttribute.bind(null, 'id');

test('Player button renders empty if audiotrack is null', () => {
    const audioTrack = JSON.stringify(TEST_AUDIO)
  
    const { container } = render(
        <Player audioTrack={null} />
    );
  
    expect(container.querySelector(".audioPlayer")).toBeNull();
});

test('Player button renders if audiotrack is provided', () => {
    const audioTrack = JSON.stringify(TEST_AUDIO)
  
    const { container } = render(
        <Player audioTrack={audioTrack} />
    );
  
    expect(container.querySelector(".audioPlayer")).not.toBeNull();
});

test('Player button renders if audiotrack is provided', () => {
    const audioTrack = JSON.stringify(TEST_AUDIO)
  
    const { container } = render(
        <Player audioTrack={audioTrack} />
    );
  
    expect(container.querySelector(".audioPlayer")).not.toBeNull();
});

test('Player shows pause icon', () => {
    const audioTrack = JSON.stringify(TEST_AUDIO)
  
    const dom = render(
        <Player audioTrack={audioTrack} />
    );

    fireEvent.click(getById(dom.container, 'playButton'))
  
    expect(getById(dom.container, 'pause')).not.toBeNull();
});

test('Player shows play icon', () => {
    const audioTrack = JSON.stringify(TEST_AUDIO)
  
    const dom = render(
        <Player audioTrack={audioTrack} />
    );
  
    expect(getById(dom.container, 'play')).not.toBeNull();
});

